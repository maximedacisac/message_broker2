<?php

namespace App\Handler;

use App\Message\OrderMessage;
use Symfony\Component\HttpKernel\Log\Logger;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;


#[AsMessageHandler]
class OrderHandler
{
    private $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function __invoke(OrderMessage $message)
    {
        file_put_contents($this->filePath, $message->getTask() . PHP_EOL, FILE_APPEND);
    }
}
