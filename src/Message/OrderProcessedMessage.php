<?php

namespace App\Message;

class OrderProcessedMessage
{
    private $orderId;
    private $processedAt;

    public function __construct(int $orderId, \DateTimeInterface $processedAt)
    {
        $this->orderId = $orderId;
        $this->processedAt = $processedAt;
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function getProcessedAt(): \DateTimeInterface
    {
        return $this->processedAt;
    }
}
