<?php

namespace App\Message;

class OrderMessage
{
    private $orderId;
    private $customerName;
    private $orderedItem;
    private $createdAt;

    public function __construct(int $orderId, string $customerName, string $orderedItem)
    {
        $this->orderId = $orderId;
        $this->customerName = $customerName;
        $this->orderedItem = $orderedItem;
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function getCustomerName(): string
    {
        return $this->customerName;
    }

    public function getOrderedItem(): string
    {
        return $this->orderedItem;
    }

    public function setCreatedAt(\DateTime $newDate)
    {
        $this->createdAt = $newDate;
    }
}
