<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Message\OrderMessage;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    #[Route('/command', methods: ['POST'])]
    public function createTask(Request $request, MessageBusInterface $messageBus): Response
    {
        try {
            $content = json_decode($request->getContent(), true);
            $orderId = $content['orderId'];
            $customerName = $content['customerName'];
            $orderedItem = $content['orderedItem'];
            $message = new OrderMessage($orderId,$customerName,$orderedItem);
            $messageBus->dispatch($message);
        } catch (\Exception $e) {
            return new Response('Error: ' . $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }

        return new Response('Commande crée', Response::HTTP_CREATED);
    }
}

