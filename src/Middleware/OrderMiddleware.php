<?php


namespace App\Middleware;

use App\Message\OrderMessage;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\StackInterface;

class OrderMiddleware implements MiddlewareInterface
{
    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        $message = $envelope->getMessage();

        if ($message instanceof OrderMessage) {
            $message->setCreatedAt(new \DateTime());
        }

        return $stack->next()->handle($envelope, $stack);
    }
}

